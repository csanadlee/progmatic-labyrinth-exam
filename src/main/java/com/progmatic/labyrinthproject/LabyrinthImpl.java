package com.progmatic.labyrinthproject;

import com.progmatic.labyrinthproject.enums.CellType;
import com.progmatic.labyrinthproject.enums.Direction;
import com.progmatic.labyrinthproject.exceptions.CellException;
import com.progmatic.labyrinthproject.exceptions.InvalidMoveException;
import com.progmatic.labyrinthproject.interfaces.Labyrinth;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/**
 *
 * @author pappgergely
 */
public class LabyrinthImpl implements Labyrinth {

    private int height=0;
    private int width =0;
    private Coordinate[][] coordz = new Coordinate[height][width];
    private Coordinate playerPosition;

    public LabyrinthImpl() {
        
    }

    @Override

    public void loadLabyrinthFile(String fileName) {
        try {
            Scanner sc = new Scanner(new File(fileName));
            int width = Integer.parseInt(sc.nextLine());
            int height = Integer.parseInt(sc.nextLine());

            this.height = height;
            this.width = width;

            for (int i = 0; i < this.height; i++) {
                for (int j = 0; j < this.width; j++) {
                    coordz[i][j].setType(CellType.EMPTY);

                }

            }

            for (int hh = 0; hh < height; hh++) {
                String line = sc.nextLine();
                for (int ww = 0; ww < width; ww++) {
                    switch (line.charAt(ww)) {
                        case 'W':
                            coordz[hh][ww].setType(CellType.WALL);
                            break;
                        case 'E':
                            coordz[hh][ww].setType(CellType.END);
                            break;
                        case 'S':
                            coordz[hh][ww].setType(CellType.START);
                            setPlayerPosition(coordz[hh][ww]);
                            break;
                    }
                }
            }
        } catch (FileNotFoundException | NumberFormatException ex) {
            System.out.println(ex.toString());

        }

    }

    @Override
    public int getHeight() {
        if (height != 0) {
            return height;
        } else {
            return -1;

        }
    }

    @Override
    public int getWidth() {
        if (width != 0) {
            return width;
        } else {
            return -1;

        }
    }

    @Override
    public CellType getCellType(Coordinate c) throws CellException {

        CellException cellexception = new CellException(c.getRow(), c.getCol(), "valami nem döfi - a koordináta_lekérdezésnél; - például nincs ilyen indexünk");

        if (c.getRow() > height || c.getRow() < 0 || c.getCol() > width || c.getCol() < 0) {

            throw cellexception;
        } else {
            return coordz[c.getRow()][c.getCol()].getType();
        }

    }

    public void setPlayerPosition(Coordinate playerPosition) {
        this.playerPosition = playerPosition;
    }

    @Override
    public void setSize(int width, int height) {
        this.height = height;
        this.width = width;
    }

    @Override
    public void setCellType(Coordinate c, CellType type) throws CellException {
        CellException cellexception = new CellException(c.getRow(), c.getCol(), "valami nem döfi - a koordináta_belállításnál; - például nincs ilyen indexünk");

        if (c.getRow() > height || c.getRow() < 0 || c.getCol() > width || c.getCol() < 0) {
            throw cellexception;
        } else {
            coordz[c.getRow()][c.getCol()].setType(type);
        }

    }

    public Coordinate[][] getCoordz() {
        return coordz;
    }

    @Override
    public Coordinate getPlayerPosition() {
        return playerPosition;
    }

    @Override
    public boolean hasPlayerFinished() {
        if (getPlayerPosition().getType().equals(CellType.END)) {
            return true;
        } else {
            return false;
        }
    }

    @Override
    public List<Direction> possibleMoves() {

        List<Direction> possibleMoves = new ArrayList<>();

        Coordinate position = getPlayerPosition();

        if (!coordz[position.getRow() - 1][position.getCol()].getType().equals(CellType.WALL)) {
            possibleMoves.add(Direction.NORTH);
        }
        if (!coordz[position.getRow() + 1][position.getCol()].getType().equals(CellType.WALL)) {
            possibleMoves.add(Direction.SOUTH);
        }
        if (!coordz[position.getRow()][position.getCol() - 1].getType().equals(CellType.WALL)) {
            possibleMoves.add(Direction.WEST);
        }
        if (!coordz[position.getRow()][position.getCol() + 1].getType().equals(CellType.WALL)) {
            possibleMoves.add(Direction.EAST);
        }
        return possibleMoves;
    }

    @Override
    public void movePlayer(Direction direction) throws InvalidMoveException {

        InvalidMoveException invalidmove = new InvalidMoveException();
        
        Coordinate position=getPlayerPosition();
        Coordinate new_pos=getPlayerPosition();
        List<Direction> okToGo = possibleMoves();
        for (Direction direction1 : okToGo) {
            if (direction1.equals(direction)) {
                
                switch(direction){

                    case NORTH:
                            new_pos= coordz[position.getRow() - 1][position.getCol()];
                           break;
                    case SOUTH:
                            new_pos= coordz[position.getRow() + 1][position.getCol()];
                            break;
                    case EAST:       
                            new_pos= coordz[position.getRow()][position.getCol()+1];
                            break;
                    case WEST:
                            new_pos= coordz[position.getRow()][position.getCol()-1];
                            break;
                
               
                }
                setPlayerPosition(new_pos);
            
            }else {
                throw invalidmove;
        }

    }

}

}
